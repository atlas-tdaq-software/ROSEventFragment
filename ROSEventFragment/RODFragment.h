// $Id$
#ifndef RODFRAGMENT_H
#define RODFRAGMENT_H

#include "emon/SelectionCriteria.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSObjectAllocation/FastObjectPool.h"

namespace emon 
{
  class SelectionCriteria;
}

namespace ROS 
{
  class RODFragment : public EventFragment
  {
  public:
    typedef struct 
    {
      u_int startOfHeaderMarker;
      u_int headerSize;
      u_int formatVersionNumber;
      u_int sourceIdentifier;
      u_int runNumber;
      u_int level1Id;
      u_int bunchCrossingId;
      u_int level1TriggerType;
      u_int detectorEventType;
    } RODHeader;

    typedef struct 
    {
      u_int numberOfStatusElements;
      u_int numberOfDataElements;
      u_int statusBlockPosition;
    } RODTrailer;
    
    static const u_int s_rodheaderSize = sizeof(RODHeader) / sizeof(u_int);
    static const u_int s_rodtrailerSize = sizeof(RODTrailer) / sizeof(u_int);

  private:
    Buffer *m_buffer;
    RODHeader *m_header;
    RODTrailer *m_trailer;
    u_int *m_body;
    u_int *m_statbuf;
    u_int m_numberOfDataElements;
    u_int m_numberOfStatusElements;
    u_int m_statusBlockPosition;
    std::vector<u_int> m_hwstatus;          //status words of appended HW fragments 
		 
  public:
    RODFragment(MemoryPool *mempool);

    // Special constructor to map a ROD fragment on a data buffer
    RODFragment(Buffer* buffer);

    // Special constructor for emulated event fragments (RCD)
    RODFragment(MemoryPool *buffer, 
                u_int sourceId,
		u_int level1Id, 
		u_int bunchCrossingId,
		u_int level1TriggerType,
		u_int detectorEventType,
		u_int numberOfStatusElements,
		u_int statusBlockPosition,
                u_int runNumber = 0);

    virtual ~RODFragment();
    virtual bool fragmentReady();
    virtual u_int size();
    virtual u_int level1Id();
    virtual u_int runNumber();     
    virtual u_int status();
    void setStatus(u_int status_word); 
    u_int check();
    virtual Buffer *buffer () const;
    bool selectionCriteriaMatch(emon::SelectionCriteria *sc); 
    void append(EventFragment* hwFragment);
    void appendTrailer(u_int statusBlockPosition, u_int numberOfStatusElements);
    void appendStatusAndTrailer(std::vector <u_int> *status);
    void appendStatusAndTrailer(void);
    void print(std::ostream& out = std::cout) const;
    friend std::ostream& operator<<(std::ostream& out, const RODFragment & frag);

    //For faster allocation!
    void * operator new(size_t);
    void operator delete(void *memory);
  };
  
  inline bool RODFragment::fragmentReady()
  {
    return(true);
  }

  inline void * RODFragment::operator new(size_t) 
  {
    return FastObjectPool<RODFragment>::allocate(); //This one is thread UNSAFE
  }
  
  inline void RODFragment::operator delete(void *memory) 
  {
    FastObjectPool<RODFragment>::deallocate(memory); //This one is thread UNSAFE
  }
  
  inline u_int RODFragment::size() 
  {
    return (m_buffer->size() / sizeof (u_int));
  }
  
  inline u_int RODFragment::level1Id() 
  {
    return (m_header->level1Id);
  }
  
  inline u_int RODFragment::status() 
  {
    return (0);  // Just return 0 for now
  }
  
  inline void RODFragment::setStatus(u_int) 
  {
    return;
  }
  
  inline Buffer* RODFragment::buffer() const 
  {
    return (m_buffer);
  }
}
#endif //RODFRAGMENT_H
