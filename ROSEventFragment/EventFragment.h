// -*- c++ -*- $Id$

#ifndef EVENTFRAGMENT_H
#define EVENTFRAGMENT_H

#include <sys/types.h>
#include "ROSEventFragment/EventFragmentException.h"

namespace emon 
{
  class SelectionCriteria;
}
namespace ROS 
{
   class Buffer;
   //! Abstract event fragment class.
   class EventFragment
   {
   public:  
      //Status codes. See event format document
      enum
      {
        STATUS_BCID     = 1,
        STATUS_EVID     = 2,
        STATUS_TIMEOUT  = 4,
        STATUS_ILLDATA  = 8,
        STATUS_OVERFLOW = 16,
	STATUS_SWDUMMY  = 32,
	STATUS_MAYCOME  = 0x40000004,
	STATUS_LOST     = 0x20000004
      };

      static const u_int ros_size_offset = 1;
      static const u_int ros_header_size_offset = 2;
      static const u_int ros_l1id_offset = 1;
      static const u_int rod_l1id_offset = 5;
      static const u_int full_l1id_offset = 15;
      static const u_int rod_bcid_offset = 6;
      
      typedef struct 
      {
         u_int startOfHeaderMarker;
         u_int totalFragmentsize;
         u_int headerSize;
         u_int formatVersionNumber;
         u_int sourceIdentifier;
         u_int numberOfStatusElements;
      }  GenericHeader;  //MJ: do I have to add the status elements array here?

      virtual u_int size() = 0;
      virtual u_int level1Id() = 0;
      virtual u_int status() = 0;
      virtual void setStatus(u_int status_word) = 0;
      virtual u_int runNumber() = 0;
      virtual bool fragmentReady() = 0;
      virtual Buffer* buffer(void) const = 0;
      virtual void print(std::ostream& out = std::cout) const = 0;
      virtual ~EventFragment() { }
      virtual bool selectionCriteriaMatch(emon::SelectionCriteria*) 
      {
        return true;
      }

      static const u_int s_formatVersionNumber    = 0x05000000;
      static const u_int s_rodformatVersionNumber = 0x03010000;
      static const u_int s_rodMarker              = 0xee1234ee;
      static const u_int s_robMarker              = 0xdd1234dd;
      static const u_int s_fullMarker             = 0xaa1234aa;
   };
}
#endif //EVENTFRAGMENT_H

