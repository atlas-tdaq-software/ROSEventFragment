// -*- c++ -*-
/*
  ATLAS TEST Software

  Class: EVENTFFRAGMENTEPTION
  Authors: ATLAS ROS group 	
*/

#ifndef EVENTFFRAGMENTEPTION_H
#define EVENTFFRAGMENTEPTION_H

#include "DFExceptions/ROSException.h"
#include <string>
#include <iostream>

class EventFragmentException : public ROSException 
{  
  public:
    enum ErrorCode {ROB_NOROD, ROS_BAD_BCID, ROS_BAD_L1ID, ROS_BAD_RUNN, ROD_ILLSTAT};   
    EventFragmentException(ErrorCode error, std::string description);
    EventFragmentException(ErrorCode error);
    EventFragmentException(ErrorCode error, std::string description, const ers::Context& context);
    EventFragmentException(ErrorCode error, const ers::Context& context);
    EventFragmentException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context);

    virtual ~EventFragmentException() throw() { }
  protected:
    virtual std::string getErrorString(u_int errorId) const;
    virtual ers::Issue * clone() const { return new EventFragmentException( *this ); }
    static const char * get_uid() { return "ROS::EventFragmentException"; }
    virtual const char * get_class_name() const { return get_uid(); }
};

inline EventFragmentException::EventFragmentException(EventFragmentException::ErrorCode error)
  : ROSException("ROSEventFragment", error, getErrorString(error)) { }

inline EventFragmentException::EventFragmentException(EventFragmentException::ErrorCode error, std::string description) 
  : ROSException("ROSEventFragment", error, getErrorString(error), description) { }
inline EventFragmentException::EventFragmentException(EventFragmentException::ErrorCode error, const ers::Context& context)
  : ROSException("ROSEventFragment", error, getErrorString(error), context) { }

inline EventFragmentException::EventFragmentException(EventFragmentException::ErrorCode error, std::string description, const ers::Context& context)
  : ROSException("ROSEventFragment", error, getErrorString(error), description, context) { }

inline EventFragmentException::EventFragmentException(const std::exception& cause, ErrorCode error, std::string description, const ers::Context& context) 
     : ROSException(cause, "ROSEventFragment", error, getErrorString(error), description, context) {}

inline std::string EventFragmentException::getErrorString(u_int errorId) const 
{
  std::string rc;
      
  switch (errorId) 
  {
    case  ROB_NOROD:
      rc = "The requested information (runNumber, L1ID, BCID) cannot be provided becase the ROB fragment does not have a ROD fragment yet"; break;
    case ROS_BAD_BCID:
      rc = "Bunch crossing ID does not match"; break;
    case ROS_BAD_L1ID:
      rc = "Level 1 ID does not match"; break;     
    case ROS_BAD_RUNN:
      rc = "run number does not match"; break;
    case ROD_ILLSTAT:
      rc = "Status block is not in the right position (i.e. after the data)"; break;
    default:
      rc = ""; break;
  }
  return rc;
}

#endif //EVENTFFRAGMENTEPTION_H
 
