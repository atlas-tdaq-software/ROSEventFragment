// -*- c++ -*-  $Id$
/*
  ATLAS ROS Software

  Class: FULLFRAGMENT
  Author: Markus Joos 	
*/

#ifndef FULLFRAGMENT_H
#define FULLFRAGMENT_H

#include "emon/SelectionCriteria.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSMemoryPool/MemoryPool.h"

namespace emon 
{
  class SelectionCriteria;
}

namespace ROS 
{
  class Buffer;
  class ROBFragment;
  class VFragment;
  class FullFragment : public EventFragment
  {
  public:
     static const u_int s_nStatusElements = 1; 
     typedef struct 
     {
       GenericHeader generic;
       u_int statusElement[s_nStatusElements];
       u_int crc_flag;
       u_int bcTime1;
       u_int bcTime2;
       u_int gid_ls;
       u_int gid_ms;                          
       u_int runType;
       u_int runNumber;
       u_int luminosityBlockNumber;
       u_int l1id;
       u_int bcid;
       u_int level1tt;
       u_int compressionType;                 
       u_int uncompressedPayloadSize;         
       u_int numberOfLevel1TriggerInfoWords;  //MJ: we only support 0
       u_int numberOfLevel2TriggerInfoWords;  //MJ: we only support 0
       u_int numberOfEventFilterInfoWords;    //MJ: we only support 0
       u_int numberOfStreamTagWords;          //MJ: we only support 0
     } FullHeader;
     
     FullFragment(MemoryPool* mempool, u_int runNumber, u_int sourceId);
     FullFragment(MemoryPage *mem_page);                 
     virtual ~FullFragment();
     virtual bool fragmentReady();
     u_int size();
     u_int level1Id();
     u_int level1tt();
     u_int det();
     u_int bcid();
     u_int sourceIdentifier();
     u_int runNumber();     
     u_int status();
     void setStatus(u_int status_word); 
     u_int check();
     void append(ROBFragment*);
     void append(FullFragment*);
     void print(std::ostream& out = std::cout) const;
     bool selectionCriteriaMatch(emon::SelectionCriteria *sc);      
     void init_lumi(void);
     int get_lumi();     
     friend std::ostream& operator<<(std::ostream& out, const FullFragment & frag);

     Buffer* buffer(void) const;

     //For faster allocation!
     void * operator new(size_t);
     void operator delete(void *memory);

  private:
     Buffer *m_buffer;
     FullHeader *m_FullHeader;
     u_int m_firstappend;
     u_int m_rod_detector_event_type;
  };

  inline bool FullFragment::fragmentReady()
  {
    return(true);
  }

  inline u_int FullFragment::size() 
  {
    return (m_FullHeader->generic.totalFragmentsize);
  }

  inline u_int FullFragment::level1tt() 
  {
    return (m_FullHeader->level1tt);
  }
  
  inline u_int FullFragment::det()
  {
    return (m_rod_detector_event_type);
  }

  inline u_int FullFragment::bcid() 
  {
    return (m_FullHeader->bcid);
  }

  inline u_int FullFragment::sourceIdentifier() 
  {
    return (m_FullHeader->generic.sourceIdentifier);
  }
  
  inline u_int FullFragment::level1Id() 
  {
    return (m_FullHeader->l1id);
  }

  inline Buffer* FullFragment::buffer(void) const 
  {
    return (m_buffer);
  }

  inline void * FullFragment::operator new(size_t) 
  {
    return FastObjectPool<FullFragment>::allocate(); //This one is Thread UNSAFE!
  }
  
  inline void FullFragment::operator delete(void * memory) 
  {
    FastObjectPool<FullFragment>::deallocate(memory); //This one is thread UNSAFE!
  }
  
  inline std::ostream& operator<<(std::ostream& out, const FullFragment & frag)
  {
    frag.print(out);
    return out;
  }
}
#endif //FULLFRAGMENT_H

