// -*- c++ -*-  $Id$

#ifndef ROBFRAGMENT_H
#define ROBFRAGMENT_H
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSEventFragment/EventFragment.h"
#include "ROSEventFragment/RODFragment.h"

namespace ROS 
{
  class Buffer;
  class RODFragment;
  class ROBFragment : public EventFragment
  {
  public:
     //The "3" is is to get a 64-bit aligned header for Filar input. ROB headers from a Robin
     //only have two status words. This has to be compensated in Robin related code
     static const u_int s_nStatusElements = 3;       

     typedef struct 
     {
       GenericHeader generic;
       u_int statusElement[s_nStatusElements];
       u_int crc_flag;
     } ROBHeader;
     
     typedef struct 
     {
       u_int robin_crc;
     } ROBTrailer;
     
     ROBFragment(MemoryPool *mempool, void *fragment);
     ROBFragment(MemoryPool *mempool); 
     ROBFragment(MemoryPage *mem_page, u_int RODFragmentSize,  
                 u_int FragmentStatus, u_int sourceId);
     ROBFragment(MemoryPage *mem_page, bool robinnp_type = false); 
     ROBFragment(MemoryPool *mempool, u_int sourceId,
                 u_int level1Id, u_int bunchCrossingId,
                 u_int level1TriggerType,
                 u_int detectorEventType,
                 u_int numberOfRODStatusElements,
                 u_int numberOfDataElements,        //The size of the data part in 32bit words
                 u_int statusBlockPosition,
		 u_int runNumber = 0);

     // Special constructor for missing event fragments 
     ROBFragment(MemoryPool* mempool, u_int level1Id, u_int sourceId, u_int runNumber = 0);

     virtual ~ROBFragment();

     virtual bool fragmentReady();
     void fill_data();
     void emulate_SLIDAS();
     u_int size();
     u_int level1Id();
     void level1Id(u_int id);
     u_int level1tt();
     u_int det();
     u_int runNumber();
     u_int sourceIdentifier();
     u_int status(); 
     u_int status(u_int index); 
     void setStatus(u_int status_word); 
     u_int check(u_int ipacket, u_int L1extdcheck);
     u_int checkstatus(u_int *status);
     u_int bunchCrossingId();
     u_long getPCIaddress();
     u_int getCRC();
     u_int clearCRC();

     void print(std::ostream& out = std::cout) const;
     Buffer* buffer(void) const;
     friend std::ostream& operator<<(std::ostream& out, const ROBFragment & frag);

     //For faster allocation!
     void * operator new(size_t);
     void operator delete(void *memory);

  private:
     static const u_int s_robheaderSize = sizeof(ROBHeader) / sizeof(u_int);
     static const u_int s_robtrailerSize = sizeof(ROBTrailer) / sizeof(u_int);

     ROBHeader* m_header;
     ROBTrailer* m_trailer;
     RODFragment::RODHeader* m_rodheader;
     RODFragment::RODTrailer* m_rodtrailer;
     u_int m_rodFragmentExists;
     u_int* m_rodbody;
     Buffer* m_buffer;

     void initialiseHeader(u_int sourceId, u_int statusElement);
  };

  inline Buffer* ROBFragment::buffer() const 
  {
    return (m_buffer);
  }

  inline u_int ROBFragment::size() 
  {
    return (m_header->generic.totalFragmentsize);
  }

  inline u_int ROBFragment::sourceIdentifier() 
  {
    return (m_header->generic.sourceIdentifier);
  }

  inline void * ROBFragment::operator new(size_t) 
  {
    return FastObjectPool<ROBFragment>::allocate() ; //This one is thread UNSAFE
  }
  
  inline void ROBFragment::operator delete(void * memory) 
  {
    FastObjectPool<ROBFragment>::deallocate(memory) ; //This one is thread UNSAFE
  }

  inline std::ostream& operator<<(std::ostream& out, const ROBFragment & frag)
  {
      frag.print(out);
    return out;
  }
}
#endif //ROBFRAGMENT_H
