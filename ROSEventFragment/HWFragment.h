// $Id$
#ifndef HWFRAGMENT_H
#define HWFRAGMENT_H

#include "ROSEventFragment/EventFragment.h"
#include "ROSMemoryPool/MemoryPool.h"
#include "ROSObjectAllocation/FastObjectPool.h"
#include "ROSBufferManagement/Buffer.h"

namespace RCD 
{
  using namespace ROS;

  class HWFragment : public ROS::EventFragment
  {
    private:
      Buffer *m_buffer;
      u_int *m_data;
      u_int m_status;

    public:
      enum
      {
	SEQUENCE = 0,
	ALTERNATE_BIT_PATTERN
      };

      HWFragment (MemoryPool* mempool);
      HWFragment (MemoryPool* mempool, u_int length, u_int mode = SEQUENCE);

      virtual ~HWFragment(void);
      virtual bool fragmentReady();
      virtual u_int size(void);
      virtual u_int level1Id(void);
      void putStatus(u_int status);
      u_int getStatus(void);
      u_int status(void); 
      void setStatus(u_int status_word); 
      u_int check(void);
      virtual Buffer *buffer() const;
      void append(EventFragment* fragment);
      void append(Buffer* buffer);
      virtual u_int runNumber(void);     
      virtual void print(std::ostream& out = std::cout) const;
      friend std::ostream& operator<<(std::ostream& out, const HWFragment & frag);

      //For faster allocation!
      void * operator new(size_t);
      void operator delete(void *memory);
  };

  inline bool HWFragment::fragmentReady()
  {
    return(true);
  }
  
  inline void * HWFragment::operator new(size_t) 
  {
    return FastObjectPool<HWFragment>::allocate(); //This one is thread UNSAFE
  }
  
  inline void HWFragment::operator delete(void * memory) 
  {
    FastObjectPool<HWFragment>::deallocate(memory); //This one is thread UNSAFE
  }
  
  inline u_int HWFragment::size() 
  {
    return (m_buffer->size () / sizeof (u_int));
  }

  inline u_int HWFragment::level1Id() 
  {
    return (0);
  }
  
  inline u_int HWFragment::getStatus(void) 
  {
    return (m_status);
  }

  inline u_int HWFragment::status() 
  {
    return (m_status);
  }
  
  inline void HWFragment::setStatus(u_int status_word) 
  {
    m_status = status_word;
  }
  
  inline void HWFragment::putStatus(u_int status) 
  {
    m_status = status;
  }
  
  inline Buffer* HWFragment::buffer() const 
  {
    return (m_buffer);
  }
}
#endif //HWFRAGMENT_H
