// $Id$

#include <iostream>
#include <sys/types.h>
#include "ROSEventFragment/HWFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "ROSBufferManagement/BufferManagementException.h"
#include "ROSUtilities/ROSErrorReporting.h"


using namespace RCD;


/*****************************************/
HWFragment::HWFragment(MemoryPool* mempool)
/*****************************************/
{
  m_buffer = new Buffer (mempool);
}


/*******************************************************************/
HWFragment::HWFragment(MemoryPool* mempool, u_int length, u_int mode)
/*******************************************************************/
{
  u_int ilength = length / sizeof (u_int);

  m_buffer = new Buffer (mempool);
  m_data   = new (m_buffer) u_int[ilength];

  m_data[0] = ilength;

  if (mode == SEQUENCE)
  {
    for (u_int i = 1; i < ilength; ++i)
      m_data[i] = i;
  }
  else
  {
    for (u_int i = 1; i < ilength; ++i)
    {
      if (i & 0x1)
        m_data[i] = 0xaaaaaaaa;
      else
        m_data[i] = 0x55555555;
    }    
  }  
}


/**********************************************/
void HWFragment::append(EventFragment *fragment)
/**********************************************/
{
  // Append hardware fragment buffer
  Buffer* buffer = const_cast<Buffer*>(fragment->buffer());
  m_buffer->append(*buffer);
}


/*************************************/
void HWFragment::append(Buffer *buffer)
/*************************************/
{
  m_buffer->append(*buffer);
}

                                                                                
/*********************************************/
void HWFragment::print(std::ostream& out) const
/*********************************************/
{
  out << "Not yet implemented" << std::endl;
}


/***************************/
u_int HWFragment::runNumber()               
/***************************/
{
  return(0xffffffff);
}


/***********************/
HWFragment::~HWFragment()
/***********************/
{
  delete m_buffer;
}
