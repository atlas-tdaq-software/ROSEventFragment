// $Id$

#include <iostream>
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSEventFragment/RODFragment.h"


using namespace ROS;


/*******************************************/
RODFragment::RODFragment(MemoryPool* mempool)
/*******************************************/
{
  m_buffer = new Buffer(mempool);
  m_header = (RODHeader *)(*(m_buffer->begin()))->address();
  m_hwstatus.clear();
  m_numberOfStatusElements = 0;
}


/**************************************/
RODFragment::RODFragment(Buffer *buffer)
/**************************************/
{
  m_buffer = buffer;
  m_header = (RODHeader *)(*(m_buffer->begin()))->address();
  m_hwstatus.clear();
  m_numberOfStatusElements = 0;

  // Get last page 
  Buffer::page_iterator last_page = m_buffer->end() - 1;
  char *last_end = (char *)(*last_page)->address() + (*last_page)->usedSize();
  m_trailer = (RODTrailer *)(last_end - sizeof (RODHeader));
}


/****************************************************/
RODFragment::RODFragment(MemoryPool *mempool, 
                         u_int sourceId,
                         u_int level1Id, 
			 u_int bunchCrossingId,
                         u_int level1TriggerType,
                         u_int detectorEventType,
                         u_int numberOfStatusElements,
                         u_int statusBlockPosition,
			 u_int runNumber)
/****************************************************/
{
  m_buffer = new Buffer(mempool);
  m_hwstatus.clear();
 
  m_header = new(m_buffer) RODHeader;
  m_header->startOfHeaderMarker = s_rodMarker;
  m_header->headerSize          = sizeof(RODHeader) / sizeof (u_int);
  m_header->formatVersionNumber = s_rodformatVersionNumber;
  m_header->sourceIdentifier    = sourceId;
  m_header->level1Id            = level1Id;
  m_header->runNumber           = runNumber;
  m_header->bunchCrossingId     = bunchCrossingId;
  m_header->level1TriggerType   = level1TriggerType;
  m_header->detectorEventType   = detectorEventType;

  if (!statusBlockPosition)
  {
    m_statbuf = new(m_buffer) u_int[numberOfStatusElements];
    m_numberOfStatusElements = numberOfStatusElements;
  }
  else
    m_numberOfStatusElements = 0;

  m_numberOfDataElements = 0;
  m_statusBlockPosition  = statusBlockPosition;
}


/***************************************************************************************/
void RODFragment::appendTrailer (u_int statusBlockPosition, u_int numberOfStatusElements)
/***************************************************************************************/
{
  u_int i;

  if (statusBlockPosition)
    m_statbuf = new(m_buffer) u_int[numberOfStatusElements];

  for (i = 0; i < numberOfStatusElements; ++i)
    m_statbuf[i] = 0;

  m_trailer = new(m_buffer) RODTrailer;
  m_trailer->numberOfStatusElements = numberOfStatusElements;
  m_trailer->numberOfDataElements   = m_numberOfDataElements;
  m_trailer->statusBlockPosition    = statusBlockPosition;
}


/*******************************************************************/
void RODFragment::appendStatusAndTrailer(std::vector <u_int> *status)
/*******************************************************************/
{
  u_int cnt;

  if (m_statusBlockPosition)
    m_statbuf = new(m_buffer) u_int[m_numberOfStatusElements];

  cnt = 0;
  for (std::vector<u_int>::const_iterator i = status->begin(); i < status->end(); i++) 
  {
    m_statbuf[cnt++] = *i;
    if (cnt > m_numberOfStatusElements)
    {
      //Problem!
    }
  }

  m_trailer = new(m_buffer) RODTrailer;
  m_trailer->numberOfStatusElements = m_numberOfStatusElements;
  m_trailer->numberOfDataElements   = m_numberOfDataElements;    
  m_trailer->statusBlockPosition    = m_statusBlockPosition;
}


/********************************************/
void RODFragment::appendStatusAndTrailer(void)
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "RODFragment::appendStatusAndTrailer: called. m_numberOfStatusElements = " << m_numberOfStatusElements); 
  if (m_statusBlockPosition)
    m_statbuf = new(m_buffer) u_int[m_numberOfStatusElements];
  else
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "RODFragment::appendStatusAndTrailer: m_statusBlockPosition = 0. Cannot append status information"); 
    CREATE_ROS_EXCEPTION(ex1, EventFragmentException, ROD_ILLSTAT, "");
    throw (ex1);
  }
  
  for (u_int cnt = 0; cnt < m_numberOfStatusElements; cnt++) 
    m_statbuf[cnt] = m_hwstatus[cnt];

  m_trailer = new(m_buffer) RODTrailer;
  m_trailer->numberOfStatusElements = m_numberOfStatusElements;
  m_trailer->numberOfDataElements   = m_numberOfDataElements;    
  m_trailer->statusBlockPosition    = m_statusBlockPosition;
}


/*************************************************/
void RODFragment::append(EventFragment *hwFragment)
/*************************************************/
{
  // Append hardware fragment buffer
  Buffer *hwBuffer = const_cast<Buffer*>(hwFragment->buffer());
  m_buffer->append(*hwBuffer);
  m_numberOfDataElements += hwFragment->size();
  m_hwstatus.push_back(hwFragment->status());
  m_numberOfStatusElements++;
}


/****************************/
u_int RODFragment::runNumber() 
/****************************/
{
  return (m_header->runNumber);
}


/************************/
u_int RODFragment::check()
/************************/
{
  // Dummy -- work out what to do later
  return (0);
}


/**********************************************/
void RODFragment::print(std::ostream& out) const
/**********************************************/
{
  out << "Not yet implemented" << std::endl;
}


/*************************/
RODFragment::~RODFragment()
/*************************/
{
   delete m_buffer;
}


/*******************************************************************/
bool RODFragment::selectionCriteriaMatch(emon::SelectionCriteria *sc) 
/*******************************************************************/
{
  u_int *status;
  
  DEBUG_TEXT(DFDB_ROSEF, 15, "RODFragment::selectionCriteriaMatch called");

  DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: sc->m_lvl1_trigger_type.m_value    = " << HEX(sc->m_lvl1_trigger_type.m_value));
  DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: sc->m_status_word.m_value          = " << HEX(sc->m_status_word.m_value));
  
  DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: m_header->detectorEventType    = " << HEX(m_header->detectorEventType));
  DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: m_header->level1TriggerType    = " << HEX(m_header->level1TriggerType));

  if (m_trailer->statusBlockPosition)
  {
    status = (u_int *)((u_long)&m_trailer->numberOfStatusElements - 4 * m_trailer->numberOfStatusElements); 
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: m_header->startOfHeaderMarker is " << HEX(m_header->startOfHeaderMarker));
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: m_header->startOfHeaderMarker is at " << HEX(&m_header->startOfHeaderMarker));
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: numberOfStatusElements is " << m_trailer->numberOfStatusElements);
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: numberOfStatusElements is at " << HEX(&m_trailer->numberOfStatusElements));
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: status is at " << HEX(&status[0]));
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: status word 0 (after data)   = " << HEX(status[0]));
  }
  else
  {
    status = (u_int *)((u_long)&m_header->detectorEventType + 4); 
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: status word 0 (before data)  = " << HEX(status[0]));
  }

  if(!sc->m_lvl1_trigger_type.m_ignore && ((u_int)sc->m_lvl1_trigger_type.m_value != m_header->level1TriggerType))
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: Level 1 Trigger Type does not match. I return false");
    return(false);
  }

  if(sc->m_status_word.m_ignore)
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: Status word ignored. No mismatch on other parameters. I return true");
    return(true);
  }

  if((u_int)sc->m_status_word.m_value != 0xffffffff && (u_int)sc->m_status_word.m_value == status[0])
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: Status Word of fragment matches. I return true");
    return(true);
  }

  if((u_int)sc->m_status_word.m_value == 0xffffffff && status[0] != 0)
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: Status Word of fragment is not zero. I return true");
    return(true);
  }

  DEBUG_TEXT(DFDB_ROSEF, 20, "RODFragment::selectionCriteriaMatch: No match");
  return(false);
}
