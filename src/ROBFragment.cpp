// $Id$

#define HOST  // for robin.h

#include <iostream>
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFDebug/DFDebug.h"
#include "ROSRobin/robin.h"
#include "ROSUtilities/ROSErrorReporting.h"

using namespace ROS;


/************************************************************************/
//Create A ROBFragment copying the data contained in a given memory space
ROBFragment::ROBFragment(MemoryPool* mempool, void* fragment)
/************************************************************************/
{
   ROBHeader* header = static_cast<ROBHeader*>(fragment);

   m_buffer = new Buffer(mempool);

   u_int* storage = new(m_buffer) u_int[header->generic.totalFragmentsize];

   memcpy(storage, fragment, header->generic.totalFragmentsize  * sizeof(int));

   m_header = (ROBHeader*) storage;
   m_rodheader = (RODFragment::RODHeader*)((u_long)m_header + header->generic.headerSize * sizeof(int));
   m_rodtrailer = (RODFragment::RODTrailer *)((u_long)m_header + (m_header->generic.totalFragmentsize - RODFragment::s_rodtrailerSize) * sizeof(int));
   
   if (m_header->generic.numberOfStatusElements == 2) //Compensate 3 status elements
   {
     if(m_header->statusElement[2])
       m_trailer = (ROBTrailer*)(u_long)m_rodtrailer + sizeof(int);
   }
   else
   {
     if(m_header->crc_flag)
       m_trailer = (ROBTrailer*)(u_long)m_rodtrailer + sizeof(int);
   }
   m_rodFragmentExists = 1;
}


/***********************************************************/
// Create a memory page with no content (not even the header)
ROBFragment::ROBFragment(MemoryPool* mempool)
/***********************************************************/
{
   DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment constructor");
   m_buffer = new Buffer(mempool);
   m_rodFragmentExists = 0;
   m_header = (ROBHeader *)(*(m_buffer->begin()))->address();
   //MJ: ATTENTION! We cannot assign an address to the m_trailer yet
}


/****************************************************************************/
// Wrap a ROBFragment around an existing MemoryPage (assuming that it has been
// prefilled with ROD Fragment data and that the space for the ROB Header has
// been allocated before it) 
ROBFragment::ROBFragment(MemoryPage *mem_page, u_int RODFragmentSize, 
                         u_int FragmentStatus, u_int sourceId)                 
/****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::ROBFragment(S/W RobIn) called");

  m_buffer = new Buffer(mem_page);

  m_header = static_cast<ROBHeader*>(mem_page->address());
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(S/W RobIn): m_header is at " << m_header);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(S/W RobIn): RODFragmentSize = " << RODFragmentSize);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(S/W RobIn): used size = " << mem_page->usedSize());
  
  m_header->generic.startOfHeaderMarker      = s_robMarker;
  m_header->generic.headerSize               = s_robheaderSize;
  m_header->generic.formatVersionNumber      = s_formatVersionNumber;
  m_header->generic.sourceIdentifier         = sourceId & 0xffffff;    
  m_header->generic.totalFragmentsize        = s_robheaderSize + RODFragmentSize; 
  m_header->generic.numberOfStatusElements   = s_nStatusElements;
  m_header->statusElement[0]                 = FragmentStatus & 0xffff; //keep only generic status 
  for (u_int loop = 1; loop < s_nStatusElements; loop++)
    m_header->statusElement[loop]            = 0;

  m_header->crc_flag = 0;  //The header of this fragment has 3 status words. Therefore we do not have to compensate for that

  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(S/W RobIn): s_robheaderSize = " << s_robheaderSize);
  m_rodheader = (RODFragment::RODHeader*)((u_long)m_header + s_robheaderSize * sizeof(int));
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(S/W RobIn): m_rodheader is at " << m_rodheader);

  m_rodtrailer = (RODFragment::RODTrailer *)((u_long)m_header + (s_robheaderSize + RODFragmentSize - RODFragment::s_rodtrailerSize) * sizeof(int));
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(S/W RobIn): m_rodtrailer is at " << m_rodtrailer);

  //No ROB trailer. crc_flag is 0
  
  m_rodFragmentExists = 1;  
  DEBUG_TEXT(DFDB_ROSEF, 10, "ROBFragment::ROBFragment(S/W RobIn) Event " << m_rodheader->level1Id << " created");
}


/****************************************************************************/
// Wrap a ROBFragment around an existing MemoryPage (assuming that it has been
// prefilled with a complete ROB Fragment. Used by the Robin S/W 
ROBFragment::ROBFragment(MemoryPage *mem_page, bool robinnp_type)
/****************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::ROBFragment(RobIn) called with robinnp_type = " << robinnp_type);

  m_buffer = new Buffer(mem_page);

  m_header = static_cast<ROBHeader*>(mem_page->address());
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): m_header is at " << m_header);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): used size = " << mem_page->usedSize());

  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): s_robheaderSize = " << s_robheaderSize);
  if(m_header->generic.totalFragmentsize > m_header->generic.headerSize)
  {
    if (robinnp_type)
    {
      DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn) not compensating s_nStatusElements = 3 for RobinNP");
      m_rodheader = (RODFragment::RODHeader*)((u_long)m_header + (s_robheaderSize) * sizeof(int));
    }
    else
    {
      DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn) compensating s_nStatusElements = 3");
      m_rodheader = (RODFragment::RODHeader*)((u_long)m_header + (s_robheaderSize - 1) * sizeof(int));  //MJ: to compensate s_nStatusElements = 3
    }
    
    DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn) m_rodheader is at " << m_rodheader);


    if(m_header->crc_flag)
    {
      DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): ROB fragment has CRC");
      m_rodtrailer = (RODFragment::RODTrailer *)((u_long)m_header + (m_header->generic.totalFragmentsize - RODFragment::s_rodtrailerSize - ROBFragment::s_robtrailerSize) * sizeof(int));
    }
    else
    {
      DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): ROB fragment has no CRC");
      m_rodtrailer = (RODFragment::RODTrailer *)((u_long)m_header + (m_header->generic.totalFragmentsize - RODFragment::s_rodtrailerSize) * sizeof(int));
    }

    DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): m_rodtrailer is at " << m_rodtrailer);

    m_trailer = (ROBTrailer*)((u_long)m_rodtrailer + RODFragment::s_rodtrailerSize * sizeof(int));
    DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(RobIn): m_trailer is at " << m_trailer);
    
    m_rodFragmentExists = 1;
  }
  else
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::ROBFragment(RobIn): ERROR: Naked ROB header received");
  }
  
  m_rodFragmentExists = 1;
  DEBUG_TEXT(DFDB_ROSEF, 10, "ROBFragment::ROBFragment(RobIn): Event " << m_rodheader->level1Id << " created");
}


/*************************************************************/
//Create a new ROBFragment containing a ROD fragment
ROBFragment::ROBFragment(MemoryPool* mempool, u_int sourceId,
                         u_int level1Id, u_int bunchCrossingId,
                         u_int level1TriggerType,
                         u_int detectorEventType,
                         u_int numberOfRODStatusElements,
                         u_int numberOfDataElements,
                         u_int statusBlockPosition,
			 u_int runNumber)
/*************************************************************/
{
  m_buffer = new Buffer(mempool);

  // Build the ROB header
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::ROBFragment: calling initialiseHeader");
  initialiseHeader(sourceId, 0);

  // Build the ROD header
  m_rodheader = new(m_buffer) RODFragment::RODHeader;
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment: m_rodheader is at " << m_rodheader);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment: s_rodformatVersionNumber for ROD header is " << s_rodformatVersionNumber);

  m_rodheader->startOfHeaderMarker = s_rodMarker;
  m_rodheader->headerSize          = sizeof(RODFragment::RODHeader) / sizeof (u_int);
  m_rodheader->formatVersionNumber = s_rodformatVersionNumber;
  m_rodheader->sourceIdentifier    = sourceId & 0xffffff;
  m_rodheader->level1Id            = level1Id;
  m_rodheader->bunchCrossingId     = bunchCrossingId;
  m_rodheader->level1TriggerType   = level1TriggerType;
  m_rodheader->detectorEventType   = detectorEventType;
  m_rodheader->runNumber           = runNumber;

  // Build the ROD body
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment: numberOfDataElements = " << numberOfDataElements);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment: numberOfRODStatusElements = " << numberOfRODStatusElements);
  m_rodbody = new(m_buffer) u_int[numberOfDataElements + numberOfRODStatusElements];
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment: m_rodbody is at " << m_rodbody);

  // Build the ROD trailer
  m_rodtrailer = new(m_buffer) RODFragment::RODTrailer;
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment: m_rodtrailer is at " << m_rodtrailer);
  m_rodtrailer->numberOfStatusElements = numberOfRODStatusElements;
  m_rodtrailer->numberOfDataElements   = numberOfDataElements;
  m_rodtrailer->statusBlockPosition    = statusBlockPosition;

  // Generic ROB header   
  int rodsize = RODFragment::s_rodheaderSize + numberOfDataElements + numberOfRODStatusElements + RODFragment::s_rodtrailerSize;           
  m_header->generic.totalFragmentsize = s_robheaderSize + rodsize;

  //No ROB trailer. crc_flag is 0

  m_rodFragmentExists = 1;
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::ROBFragment: Emulated event " << level1Id << " created");
}


/********************************************************************************************/
ROBFragment::ROBFragment(MemoryPool* mempool, u_int level1Id, u_int sourceId, u_int runNumber)
/********************************************************************************************/
{ 
  // This constructor is for the (hopefully) rare case that a ROD fragment 
  // does not get delivered by the ROL and the FragmentManager has to 
  // return an empty ROB fragment
  
  m_buffer = new Buffer(mempool);

  // Build the ROB header
  DEBUG_TEXT(DFDB_ROSEF, 15, "calling initialiseHeader");
  initialiseHeader(sourceId, STATUS_TIMEOUT);

  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::ROBFragment(lost): s_formatVersionNumber for ROD header is " << s_formatVersionNumber);
  // Build the ROD header
  m_rodheader = new(m_buffer) RODFragment::RODHeader;
  DEBUG_TEXT(DFDB_ROSEF, 20, "m_rodheader is at " << m_rodheader);
  m_rodheader->startOfHeaderMarker = s_rodMarker;
  m_rodheader->headerSize          = sizeof(RODFragment::RODHeader) / sizeof (u_int);
  m_rodheader->formatVersionNumber = s_rodformatVersionNumber;
  //The source ID of the ROD header should not be identical to that of the ROB header. As we don't know
  //it (without additional tricks in the FM) I duplicate it anyway. FIXME
  m_rodheader->sourceIdentifier    = sourceId & 0xffffff;    
  m_rodheader->level1Id            = level1Id;
  m_rodheader->bunchCrossingId     = 0;
  m_rodheader->level1TriggerType   = 0;
  m_rodheader->detectorEventType   = 0;
  m_rodheader->runNumber           = runNumber;

  // Build the ROD body (just one status word)
  m_rodbody = new(m_buffer) u_int[1];
  *m_rodbody = STATUS_TIMEOUT;    // Error status
  DEBUG_TEXT(DFDB_ROSEF, 20, "m_rodbody is at " << m_rodbody);

  // Build the ROD trailer
  m_rodtrailer = new(m_buffer) RODFragment::RODTrailer;
  DEBUG_TEXT(DFDB_ROSEF, 20, "m_rodtrailer is at " << m_rodtrailer);
  m_rodtrailer->numberOfStatusElements = 1;
  m_rodtrailer->numberOfDataElements   = 0;
  m_rodtrailer->statusBlockPosition    = 0;

  // Generic ROB header   
  int rodsize = RODFragment::s_rodheaderSize + 1 + RODFragment::s_rodtrailerSize;           
  m_header->generic.totalFragmentsize  = s_robheaderSize + rodsize;

  //No ROB trailer. crc_flag is 0

  m_rodFragmentExists = 1;
  DEBUG_TEXT(DFDB_ROSEF, 15, "Lost event " << level1Id << " created");
}


/*********************************************************************/
void ROBFragment::initialiseHeader(u_int sourceId, u_int statusElement)
/*********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "initialiseHeader called");
  m_header                                   = new(m_buffer) ROBHeader;
  m_header->generic.startOfHeaderMarker      = s_robMarker;
  m_header->generic.headerSize               = s_robheaderSize;
  m_header->generic.formatVersionNumber      = s_formatVersionNumber;
  m_header->generic.sourceIdentifier         = sourceId&0xffffff;    
  m_header->generic.totalFragmentsize        = s_robheaderSize;
  m_header->generic.numberOfStatusElements   = s_nStatusElements;
  m_header->statusElement[0]                 = statusElement; 
  for (u_int loop = 1; loop < s_nStatusElements; loop++)
    m_header->statusElement[loop]            = 0;
  m_header->crc_flag = 0; //Header has 3 status words. Nothing to be compensated
  DEBUG_TEXT(DFDB_ROSEF, 20, "m_header->generic.startOfHeaderMarker is at " << &m_header->generic.startOfHeaderMarker);
}


/*************************/
ROBFragment::~ROBFragment()
/*************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment destructor");
  delete m_buffer;
}


/*************************/
u_int ROBFragment::status()
/*************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::status: called");
  return(m_header->statusElement[0]);
}


/************************************/
u_int ROBFragment::status(u_int index)
/************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::status: called for index " << index);
  return(m_header->statusElement[index]);
}


/********************************************/
void ROBFragment::setStatus(u_int status_word)
/********************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::setStatus: called with status_word " << HEX(status_word));
  m_header->statusElement[0] = status_word;
  return;
}


/****************************/
u_int ROBFragment::runNumber()
/****************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::runNumber: called");

  if (m_rodFragmentExists == 0)
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::runNumber: The ROB header does not yet contain a ROD fragment."); 
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::runNumber: Therefore the run number cannot be determined."); 
    CREATE_ROS_EXCEPTION(ex1, EventFragmentException, ROB_NOROD, "");
    throw (ex1);
  }

  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::runNumber: run number = " << m_rodheader->runNumber);
  return(m_rodheader->runNumber);
}


/***************************/
u_int ROBFragment::level1Id() 
/***************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::level1Id: called");

  if (m_rodFragmentExists == 0)
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::level1Id: The ROB header does not yet contain a ROD fragment."); 
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::level1Id: Therefore the L1ID cannot be determined."); 
    CREATE_ROS_EXCEPTION(ex2, EventFragmentException, ROB_NOROD, "");
    throw (ex2);
  }

  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::level1Id: L1ID = " << m_rodheader->level1Id);
  return(m_rodheader->level1Id);
}


/**********************************/
void ROBFragment::level1Id(u_int id) 
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::level1Id(u_int id): called");

  if (m_rodFragmentExists == 0) 
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::level1Id: The ROB header does not yet contain a ROD fragment."); 
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::level1Id: Therefore the L1ID cannot be set."); 
    CREATE_ROS_EXCEPTION(ex22, EventFragmentException, ROB_NOROD, "");
    throw (ex22);
  }

  m_rodheader->level1Id = id;
}


/*******************************/
bool ROBFragment::fragmentReady()
/*******************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::fragmentReady: called");
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::fragmentReady: ROB fragment status = 0x" << HEX(m_header->statusElement[0]));
  if ((m_header->statusElement[0] & 0xf0000000) == enum_fragStatusPending)
    return(false);
  else  
    return(true);
}


/*************************/
u_int ROBFragment::getCRC()
/*************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::getCRC: called");  
  if (m_rodFragmentExists == 0)
    return(0);
    
  if (m_header->generic.numberOfStatusElements == 2) //Compensate 3 status elements
  {
    if(m_header->statusElement[2] == 0)
      return(0);
  }
  else
  {
    if(m_header->crc_flag == 0)
      return(0);
  }  
  return (m_trailer->robin_crc);
}


/***************************/
u_int ROBFragment::clearCRC()
/***************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::clearCRC: called");  
  
  if (m_rodFragmentExists == 0)
    return(1);
   
  //Note: The value "3" is used to refer to an existing but dummy CRC in the ROB trailer. 
  if (m_header->generic.numberOfStatusElements == 2) //Compensate 3 status elements
    m_header->statusElement[2] = 3;
  else
    m_header->crc_flag = 3;
    
  return (0);
}


/***************************/
u_int ROBFragment::level1tt()
/***************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::level1tt: called");  
  return(m_rodheader->level1TriggerType);
}  


/**********************/
u_int ROBFragment::det()
/**********************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::det: called");  
  return(m_rodheader->detectorEventType);
}  


/**********************************/
u_int ROBFragment::bunchCrossingId() 
/**********************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::bunchCrossingId: called");

  if (m_rodFragmentExists == 0)
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::bunchCrossingId: The ROB header does not yet contain a ROD fragment."); 
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::bunchCrossingId: Therefore the BCID cannot be determined."); 
    CREATE_ROS_EXCEPTION(ex3, EventFragmentException, ROB_NOROD, "");
    throw (ex3);
  }

  DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::bunchCrossingId: BCID = 0x" << HEX(m_rodheader->bunchCrossingId));
  return(m_rodheader->bunchCrossingId);
}


/********************************************************/
u_int ROBFragment::check(u_int ipacket, u_int L1extdcheck)
/********************************************************/
{
  static u_int last_l1id = 0;
  u_int min_size, exp_size, data_size, *data_ptr;
  enum {NO_ERROR, SIZE, L1ID, GLOBAL};
   
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::check called"); 
  //For now I am only checking the ROD Fragment (used in slink_dst)
  //A check for the ROB header can be added later  
    
  // Size of received packet
  DEBUG_TEXT(DFDB_ROSEF, 20, " m_header->generic.totalFragmentsize = " <<  m_header->generic.totalFragmentsize);
  
  // Do we have a ROD fragment at all?
  min_size = s_robheaderSize + RODFragment::s_rodheaderSize + RODFragment::s_rodtrailerSize + s_robtrailerSize;
  if (m_header->generic.totalFragmentsize < min_size)
  {
    DEBUG_TEXT(DFDB_ROSEF, 5, "ROBFragment::check: The ROB fragment is too small to contain a ROD fragment");
    return(SIZE);
  }
  
  data_size = m_header->generic.totalFragmentsize - m_header->generic.headerSize - RODFragment::s_rodheaderSize - RODFragment::s_rodtrailerSize;

  if (m_header->generic.numberOfStatusElements == 2) //Compensate 3 status elements
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "crc_flag is " << m_header->statusElement[2]);
    if(m_header->statusElement[2])
      data_size--; //remove ROB trailer
  }
  else
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "crc_flag is " << m_header->crc_flag);
    if(m_header->crc_flag)
      data_size--; //remove ROB trailer
  }
 
  // Compute address of ROD data
  data_ptr = (u_int *)((u_long)m_header + (s_robheaderSize + RODFragment::s_rodheaderSize) * sizeof(int));
  if (!m_rodtrailer->statusBlockPosition)
    data_ptr += m_rodtrailer->numberOfStatusElements;  //Do not check status elements

  DEBUG_TEXT(DFDB_ROSEF, 20, "m_header is at " << m_header);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROB header size  = " << m_header->generic.headerSize);
  DEBUG_TEXT(DFDB_ROSEF, 20, "s_rodheaderSize  = " << RODFragment::s_rodheaderSize);
  DEBUG_TEXT(DFDB_ROSEF, 20, "s_rodtrailerSize = " << RODFragment::s_rodtrailerSize); 
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROD header is at " << m_rodheader);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROD data size (data + status) = " << data_size << " words");
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROD data is at " << data_ptr);
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROD trailer is at " << m_rodtrailer);
  
  //Check size of received packet
  DEBUG_TEXT(DFDB_ROSEF, 20, "rodtrailer->numberOfDataElements = " << m_rodtrailer->numberOfDataElements << " at " << &m_rodtrailer->numberOfDataElements);
  DEBUG_TEXT(DFDB_ROSEF, 20, "rodtrailer->numberOfStatusElements = " << m_rodtrailer->numberOfStatusElements << " at " << &m_rodtrailer->numberOfStatusElements);
  exp_size = m_rodtrailer->numberOfDataElements + m_rodtrailer->numberOfStatusElements; 
  if (data_size != exp_size) 
  {
    std::cout << "Wrong packet size: received " << data_size << " - expected " << exp_size << std::endl << std::endl;;  
    return(SIZE);
  }

  //Check L1Id
  if (ipacket == 0 && m_rodheader->level1Id != 0)
  {
    std::cout << "Wrong data: received L1id = " << m_rodheader->level1Id << "  expexted L1ID = 0" << std::endl;
    return(L1ID);
  }
  else if (ipacket > 0 && !L1extdcheck && m_rodheader->level1Id <= last_l1id) 
  {
    std::cout << "Wrong data: received L1id (" << m_rodheader->level1Id << ") is <= than previous (" << last_l1id << ")" << std::endl << std::endl;
    last_l1id = m_rodheader->level1Id; 
    return(L1ID);
  }
  else if (ipacket > 0 && L1extdcheck)
  {
    if (m_rodheader->level1Id == ((last_l1id & 0xff000000) + 0x01000000))
      std::cout << "ECR detected: L1ID jumped from 0x" << HEX(last_l1id) << " to 0x" << HEX(m_rodheader->level1Id) << std::endl;
    else if (m_rodheader->level1Id != (last_l1id + 1)) 
    {
      std::cout << "Wrong data: received L1id (" << m_rodheader->level1Id << ") is not previous (" << last_l1id << ") + 1" << std::endl << std::endl;
      last_l1id = m_rodheader->level1Id; 
      return(L1ID);
    }
  }
  last_l1id = m_rodheader->level1Id; 
  
  //Check start of header marker
  if (m_rodheader->startOfHeaderMarker != s_rodMarker)
  {    
    std::cout << "Wrong data: received startOfHeaderMarker = " << m_rodheader->startOfHeaderMarker << std::endl;
    return(L1ID);
  }
  
  //Check header size
  if (m_rodheader->headerSize != (sizeof(RODFragment::RODHeader) >> 2))
  {    
    std::cout << "Wrong data: received headerSize = " << m_rodheader->headerSize << " (expected: " << (sizeof(RODFragment::RODHeader) >> 2) << ")" << std::endl;
    return(L1ID);
  }
  
  //Check format version number
  if ((m_rodheader->formatVersionNumber & 0xffff0000) != s_rodformatVersionNumber)
  {    
    std::cout << "Wrong data: received formatVersionNumber = " << m_rodheader->formatVersionNumber << std::endl;
    return(L1ID);
  }
  
  //More tests to be added if required
  
  return (NO_ERROR);
}


/*******************************************/
u_int ROBFragment::checkstatus(u_int *status)
/*******************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::checkstatus: Called");

  //We need at least a complete ROB header and a ROD header up to the L1ID if we want to do something usefull
  u_int minsize = (sizeof(ROBHeader) >> 2) + 6;  //L1ID is word #6 in the ROD header
  if (m_header->generic.totalFragmentsize < minsize)
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "ROBFragment::checkstatus: ROB fragment size (" << m_header->generic.totalFragmentsize << " words) is below minimal size (" << minsize << " words)");
    DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::checkstatus: Done");
    *status = 0;
    return(0);   
  }

  DEBUG_TEXT(DFDB_ROSEF, 20 ,"ROBFragment::checkstatus: m_header->generic.totalFragmentsize  = " << m_header->generic.totalFragmentsize);
  DEBUG_TEXT(DFDB_ROSEF, 20 ,"ROBFragment::checkstatus: Error in ROB status word             = 0x" << HEX(m_header->statusElement[0]));
    
  *status = m_header->statusElement[0];
  DEBUG_TEXT(DFDB_ROSEF, 15 ,"ROBFragment::checkstatus: Done");
  return(1);   
}


/***************************/
void ROBFragment::fill_data()
/***************************/
{
  u_int i, se, *p;
  
  //Where are the status elements?
  if (m_rodtrailer->statusBlockPosition == 0)
    se = 0;
  else
    se = 1;
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: se = " << se);
  
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: m_header is at " << m_header);
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: m_rodbody is at " << m_rodbody);
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: s_robheaderSize = " << s_robheaderSize);
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: s_rodheaderSize = " << RODFragment::s_rodheaderSize);
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: m_rodtrailer->numberOfDataElements = " << m_rodtrailer->numberOfDataElements);
  DEBUG_TEXT(DFDB_ROSEF, 20, "fill_data: m_rodtrailer->numberOfStatusElements = " << m_rodtrailer->numberOfStatusElements);

  p = (u_int*)((u_long)m_rodbody);
  if (se)
  {
    DEBUG_TEXT(DFDB_ROSEF, 15, "fill_data: filling in data and status words...");
    //Fill the data block
    for (i = 0 ; i < m_rodtrailer->numberOfDataElements ; i++) 
      *p++ = i;
    
    //Fill the status elements
    for (i = 0 ; i < m_rodtrailer->numberOfStatusElements ; i++) 
      *p++ = 0;
  }
  else
  {
    DEBUG_TEXT(DFDB_ROSEF, 15, "fill_data: filling in status and data words...");
    //Fill the status elements
    for (i = 0 ; i < m_rodtrailer->numberOfStatusElements ; i++) 
      *p++ = 0;
  
    //Fill the data block
    for (i = 0 ; i < m_rodtrailer->numberOfDataElements ; i++) 
      *p++ = i;
  }
}


/********************************/
void ROBFragment::emulate_SLIDAS()
/********************************/
{     
  u_int *p;
 
  DEBUG_TEXT(DFDB_ROSEF, 15, "emulate_SLIDAS entered"); 
  // Overwrites so as to emulate SLIDAS
  m_rodheader->formatVersionNumber = 0x08000003;
  m_rodheader->sourceIdentifier    = 0xa0000000;
  m_rodheader->level1TriggerType   = 0x00000002;
  m_rodheader->detectorEventType   = 0x000000de;
  DEBUG_TEXT(DFDB_ROSEF, 20, "m_rodheader is at " << m_rodheader);
  DEBUG_TEXT(DFDB_ROSEF, 20, "m_rodheader->formatVersionNumber is at 0x" << &m_rodheader->formatVersionNumber);
 
  // Status words a la SLIDAS
  p = (u_int *)((u_long)m_rodheader + RODFragment::s_rodheaderSize * sizeof(int));
  DEBUG_TEXT(DFDB_ROSEF, 20, "ROD status block is at " << p);
  *p++ = 0x0;
  *p++ = 0x4;
  *p++ = 0x0;
}


/*********************************/
u_long ROBFragment::getPCIaddress()
/*********************************/
{  
  Buffer::page_iterator i = m_buffer->begin(); 
  return ((*i)->physicalAddress());
}     


/**********************************************/
void ROBFragment::print(std::ostream& out) const
/**********************************************/
{
  u_int loop, *data_ptr, nlines;

  DEBUG_TEXT(DFDB_ROSEF, 15, "ROBFragment::print:  function called");
  out << "Dumping the ROB header" << std::endl;
  out << "Generic header:" << std::endl;
  out << "startOfHeaderMarker              = 0x" << HEX(m_header->generic.startOfHeaderMarker) << std::endl;
  out << "totalFragmentsize                = " << m_header->generic.totalFragmentsize << std::endl;
  out << "headerSize                       = " << m_header->generic.headerSize << std::endl;
  out << "formatVersionNumber              = 0x" << HEX(m_header->generic.formatVersionNumber) << std::endl;
  out << "sourceIdentifier                 = 0x" << HEX(m_header->generic.sourceIdentifier) << std::endl;        
  out << "numberOfStatusElements           = " << m_header->generic.numberOfStatusElements << std::endl;
  for (loop = 0; loop < m_header->generic.numberOfStatusElements; loop++)
    out << "statusElement[" << loop << "]                 = 0x" << HEX(m_header->statusElement[loop]) << std::endl;

  if (m_header->generic.numberOfStatusElements == 2) //Compensate 3 status elements
  {
    out << "crc_flag                         = " << m_header->statusElement[2] << std::endl;
    if(m_header->statusElement[2])
    {
      out << std::endl << "Trailer:" << std::endl;
      out << "robin_crc                        = 0x" << HEX(m_trailer->robin_crc) << std::endl;
    }
  }
  else
  {
    out << "crc_flag                         = " << m_header->crc_flag << std::endl;
    if(m_header->crc_flag)
    {
      out << std::endl << "Trailer:" << std::endl;
      out << "robin_crc                        = 0x" << HEX(m_trailer->robin_crc) << std::endl;
    }
  }

  if (m_rodFragmentExists)
  {
    out << std::endl << "ROD header:" << std::endl;
    out << "startOfHeaderMarker              = 0x" << HEX(m_rodheader->startOfHeaderMarker) << std::endl;
    out << "headerSize                       = " << m_rodheader->headerSize << std::endl;
    out << "formatVersionNumber              = 0x" << HEX(m_rodheader->formatVersionNumber) << std::endl;
    out << "sourceIdentifier                 = 0x" << HEX(m_rodheader->sourceIdentifier) << std::endl;
    out << "runNumber                        = " << m_rodheader->runNumber << std::endl;
    out << "level1Id                         = " << m_rodheader->level1Id << std::endl;
    out << "bunchCrossingId                  = " << m_rodheader->bunchCrossingId << std::endl;
    out << "level1TriggerType                = 0x" << HEX(m_rodheader->level1TriggerType) << std::endl;
    out << "detectorEventType                = 0x" << HEX(m_rodheader->detectorEventType) << std::endl;

    out << std::endl << "ROD trailer:" << std::endl;
    out << "numberOfStatusElements           = " << m_rodtrailer->numberOfStatusElements << std::endl;
    out << "numberOfDataElements             = " << m_rodtrailer->numberOfDataElements << std::endl;
    out << "statusBlockPosition              = " << m_rodtrailer->statusBlockPosition << std::endl;

    data_ptr = (u_int*)((u_long)m_header + (m_header->generic.headerSize + m_rodheader->headerSize) * sizeof(int));
    DEBUG_TEXT(DFDB_ROSEF, 20, "data_ptr is at " << data_ptr);

    if (m_rodtrailer->numberOfDataElements > 4)
    {
      out << "Dumping start of ROD data" << std::endl;
      nlines = m_rodtrailer->numberOfDataElements / 4;
      if (nlines > 10)
        nlines = 10;
      for (loop = 0; loop < nlines; loop++)
	out << "0x" << std::setw(2) << HEX(loop * 4) << ":"
	<< "  0x" << std::setw(8) << HEX(data_ptr[loop * 4])
	<< "  0x" << std::setw(8) << HEX(data_ptr[loop * 4 + 1])
	<< "  0x" << std::setw(8) << HEX(data_ptr[loop * 4 + 2])
	<< "  0x" << std::setw(8) << HEX(data_ptr[loop * 4 + 3])
	<< std::endl;
    }
    else
    {
      out << "Dumping ROD data" << std::endl;
      
      if(m_rodtrailer->numberOfDataElements > 0)
        out << "0x0:" << "  0x" << std::setw(8) << HEX(data_ptr[1]) << std::endl;
      if(m_rodtrailer->numberOfDataElements > 1)
        out << "0x0:" << "  0x" << std::setw(8) << HEX(data_ptr[2]) << std::endl;
      if(m_rodtrailer->numberOfDataElements > 2)
        out << "0x0:" << "  0x" << std::setw(8) << HEX(data_ptr[3]) << std::endl;
      if(m_rodtrailer->numberOfDataElements > 3)
        out << "0x0:" << "  0x" << std::setw(8) << HEX(data_ptr[4]) << std::endl;
    }
  }
}



