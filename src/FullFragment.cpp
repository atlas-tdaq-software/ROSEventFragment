// $Id$
// //////////////////////////////////////////////////////////////////////
//  
//  Author:  Markus Joos
//
// //////////////////////////////////////////////////////////////////////
#include <sys/time.h>
#include <iostream>
#include <stdio.h>
#include "ers/ers.h"
#include "ipc/core.h"
#include "ipc/partition.h"
#include "is/inforeceiver.h"
#include "is/infoT.h"
#include "TTCInfo/LumiBlock.h"

#include "ROSBufferManagement/Buffer.h"
#include "ROSBufferManagement/BufferManagementException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSEventFragment/RODFragment.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSEventFragment/FullFragment.h"


using namespace ROS;


//Static variables
int lumi_init = 0;
int S_LumiBlockNumber = 0;
ISInfoReceiver *infoRec;

/******************************************************************************/
FullFragment::FullFragment(MemoryPool* mempool, u_int runNumber, u_int sourceId)
/******************************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment constructor called");
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment constructor: runNumber " << runNumber << " sourceId = " << sourceId);

  if (!lumi_init)
    init_lumi();

  m_buffer = new Buffer(mempool);
  // Allocate memory for all the bits of header in our buffer

  int headerSize = sizeof(FullHeader);
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment constructor: headerSize = " << headerSize);
  void* bufferAddress = m_buffer->reserve(headerSize);
  m_FullHeader = static_cast<FullHeader*>(bufferAddress);

  // Fill in as much as possible of the header
  m_FullHeader->generic.startOfHeaderMarker      = s_fullMarker;
  m_FullHeader->generic.headerSize               = headerSize / sizeof(u_int);
  m_FullHeader->generic.formatVersionNumber      = s_formatVersionNumber;
  m_FullHeader->generic.totalFragmentsize        = m_FullHeader->generic.headerSize;
  m_FullHeader->generic.sourceIdentifier         = sourceId & 0xffffff;
  m_FullHeader->generic.numberOfStatusElements   = s_nStatusElements;
  m_FullHeader->statusElement[0]                 = 0;
  m_FullHeader->crc_flag                         = 0;
  
  struct timeval tv;
  gettimeofday(&tv, 0);
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment constructor: tv.tv_sec = " << tv.tv_sec);
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment constructor: tv.tv_usec = " << tv.tv_usec);
  m_FullHeader->bcTime1                          = tv.tv_sec;
  m_FullHeader->bcTime2                          = tv.tv_usec * 1000;
  m_FullHeader->gid_ls                           = 0;
  m_FullHeader->gid_ms                           = 0;
  m_FullHeader->runType                          = 0xf;
  m_FullHeader->runNumber                        = runNumber;
  m_FullHeader->luminosityBlockNumber            = get_lumi();
  m_FullHeader->l1id                             = 0xffffffff;
  m_FullHeader->bcid                             = 0xffffffff;
  m_FullHeader->level1tt                         = 0xffffffff;
  m_FullHeader->compressionType                  = 0;
  m_FullHeader->uncompressedPayloadSize          = 0;   
  m_FullHeader->numberOfLevel1TriggerInfoWords   = 0;  
  m_FullHeader->numberOfLevel2TriggerInfoWords   = 0;  
  m_FullHeader->numberOfEventFilterInfoWords     = 0;   
  m_FullHeader->numberOfStreamTagWords           = 0;                      
  
  m_firstappend             = 1;
  m_rod_detector_event_type = 0;

  DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment constructor done");
}


/**********************************************/
FullFragment::FullFragment(MemoryPage *mem_page)                 
/**********************************************/
{
  //evsize is in words
 
  if (!lumi_init)
   init_lumi();
 
  //Here we assume that the FullFragment header has not yet been initialized
  DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment::FullFragment(EB ROS) called");

  m_buffer = new Buffer(mem_page);
  m_FullHeader = static_cast<FullHeader*>(mem_page->address());

  struct timeval tv;
  gettimeofday(&tv, 0);
  //std::cout << "tv.tv_sec = " << tv.tv_sec << std::endl;
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::FullFragment(EB ROS): tv.tv_sec = " << tv.tv_sec);
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::FullFragment(EB ROS): tv.tv_usec = " << tv.tv_usec);
  m_FullHeader->bcTime1                          = tv.tv_sec;
  m_FullHeader->bcTime2                          = tv.tv_usec * 1000;

  m_firstappend             = 1;
  m_rod_detector_event_type = 0;  //As this Full fragment may contain contributions from different detectors we 
                                  //cannot defined a detector event type
    
  DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::FullFragment(EB ROS) Event created");
}


/***************************/
FullFragment::~FullFragment()
/***************************/
{
   DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment destructor called");
   delete m_buffer;
   
   
   // According to wainer a shared pointer could be used to detect when the last fragment gets destructed
   // See:
   // http://www.boost.org/doc/libs/1_53_0/libs/smart_ptr/shared_ptr.htm#example
   
   /*
   try
   {
     if (lumi_init == 1) 
       infoRec->unsubscribe("LHC.StableBeamsFlag");
   }
   catch (daq::is::Exception & ex) 
   {
     std::cout << "WARNING: Exception caught in destructor of FullFragment" << std::endl;
   }
   */
  
   DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment destructor done");
}


/************************************************/
void FullFragment::append(FullFragment *fFragment)
/************************************************/
{
   DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment::append: function called for FullFragment");
      
   if (m_firstappend)  
   {
     m_FullHeader->bcid        = fFragment->bcid(); 
     m_FullHeader->l1id        = fFragment->level1Id(); 
     m_FullHeader->level1tt    = fFragment->level1tt();     
     m_FullHeader->gid_ls      = m_FullHeader->l1id;        
     m_FullHeader->gid_ms      = 0;     
     m_rod_detector_event_type = fFragment->det();

     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  level1Id = " << fFragment->level1Id());
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  size     = " << fFragment->size());
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  level1tt = " << fFragment->level1tt());
     m_firstappend = 0;
   }
   else
   {
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: Second: level1Id = " << fFragment->level1Id() << " size = " << fFragment->size());

     if(m_FullHeader->bcid != fFragment->bcid())
     {
       DEBUG_TEXT(DFDB_ROSEF, 5, "ERROR in FullFragment::append -> Bunch crossing IDs do not match");
       DEBUG_TEXT(DFDB_ROSEF, 5, "level1Id = " << fFragment->level1Id());
       DEBUG_TEXT(DFDB_ROSEF, 5, "expected BCID = " << m_FullHeader->bcid);
       DEBUG_TEXT(DFDB_ROSEF, 5, "BCID of appended fragment = " << fFragment->bcid());
       DEBUG_TEXT(DFDB_ROSEF, 5, "sourceIdentifier of appended fragment = 0x" << HEX(fFragment->sourceIdentifier()));
       //Set the BCID error bit in the status word
       m_FullHeader->statusElement[0] |= 0x1;
     }
   }
      
   // Append Full fragment buffer
   Buffer *fBuffer = const_cast<Buffer*>(fFragment->buffer());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: fBuffer->size() = " << fBuffer->size());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: fBuffer->reserved() = " << fBuffer->reserved());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: before append: m_buffer->size() = " << m_buffer->size());

   m_buffer->append(*fBuffer, sizeof(FullHeader));
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: append done with offset = 0x" << HEX(sizeof(FullHeader)));
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: after append: m_buffer->size() = " << m_buffer->size());

   // Update the total size
   m_FullHeader->generic.totalFragmentsize += (fFragment->size() - (sizeof(FullHeader) >> 2));    
   m_FullHeader->uncompressedPayloadSize   += (fFragment->size() - (sizeof(FullHeader) >> 2));  
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: after append: m_FullHeader->generic.totalFragmentsize = " << m_FullHeader->generic.totalFragmentsize);
}


/*************************************************/
void FullFragment::append(ROBFragment *robFragment)
/*************************************************/
{
   DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment::append: function called for ROBFragment");
      
   if (m_firstappend)  
   {
     if (robFragment->status() == STATUS_TIMEOUT)
     {
        DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  Appending lost fragment with level1Id = " << robFragment->level1Id());
        DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  robFragment->status() = " << robFragment->status());
        DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  robFragment->size() = " << robFragment->size());
     }
     
     //MJ: Where does one get the proper trigger type from. Should one dig it ouf of the header of a ROD fragment?
     m_FullHeader->bcid        = robFragment->bunchCrossingId(); 
     m_FullHeader->l1id        = robFragment->level1Id(); 
     m_FullHeader->level1tt    = robFragment->level1tt();     
     m_FullHeader->gid_ls      = m_FullHeader->l1id;     
     m_FullHeader->gid_ms      = 0;                                
     m_rod_detector_event_type = robFragment->det();
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  level1Id = " << robFragment->level1Id());
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  size     = " << robFragment->size());
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: First:  level1tt = " << robFragment->level1tt());
     m_firstappend = 0;
   }
   else
   {
     DEBUG_TEXT(DFDB_ROSEF, 10, "FullFragment::append: Second: level1Id = " << robFragment->level1Id() << " size = " << robFragment->size());

     if(m_FullHeader->bcid != robFragment->bunchCrossingId())
     {
       DEBUG_TEXT(DFDB_ROSEF, 5, "ERROR in FullFragment::append -> Bunch crossing IDs do not match");
       DEBUG_TEXT(DFDB_ROSEF, 5, "level1Id = " << robFragment->level1Id());
       DEBUG_TEXT(DFDB_ROSEF, 5, "expected BCID = " << m_FullHeader->bcid);
       DEBUG_TEXT(DFDB_ROSEF, 5, "BCID of ROB fragment = " << robFragment->bunchCrossingId());
       DEBUG_TEXT(DFDB_ROSEF, 5, "sourceIdentifier of new ROB fragment = 0x" << HEX(robFragment->sourceIdentifier()));
       //Set the BCID error bit in the status word
       m_FullHeader->statusElement[0] |= 0x1;
     }
   }
      
   // Append ROB fragment buffer
   Buffer *robBuffer = const_cast<Buffer*>(robFragment->buffer());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: robBuffer->size() = " << robBuffer->size());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: robBuffer->reserved() = " << robBuffer->reserved());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: before append: m_buffer->size() = " << m_buffer->size());

   m_buffer->append(*robBuffer); 
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: append done");

   // Update the total size
   m_FullHeader->generic.totalFragmentsize += robFragment->size();   
   m_FullHeader->uncompressedPayloadSize   += robFragment->size();     
   
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: after append: m_buffer->size() = " << m_buffer->size());
   DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::append: after append: m_FullHeader->generic.totalFragmentsize = " << m_FullHeader->generic.totalFragmentsize);
}


/**************************/
u_int FullFragment::status()
/**************************/
{
  return (m_FullHeader->statusElement[0]);
}


/*********************************************/
void FullFragment::setStatus(u_int status_word)
/*********************************************/
{
  m_FullHeader->statusElement[0] = status_word;
}


/*************************/
u_int FullFragment::check()
/*************************/
{
   DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment::check called");
   // Dummy -- work out what to do later
   //std::cout << "To be implemented" << std::endl;
   return (0);
}


/*****************************/
u_int FullFragment::runNumber() 
/*****************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment::runNumber: run number = " << m_FullHeader->runNumber);
  return (m_FullHeader->runNumber);
}


/***********************************************/
void FullFragment::print(std::ostream& out) const
/***********************************************/
{
  out << "Dumping the Full header" << std::endl;
    
  out << "Generic header:" << std::endl;
  out << "startOfHeaderMarker            = 0x" << HEX(m_FullHeader->generic.startOfHeaderMarker) << std::endl;
  out << "totalFragmentsize              = " << m_FullHeader->generic.totalFragmentsize << std::endl;
  out << "headerSize                     = " << m_FullHeader->generic.headerSize << std::endl;
  out << "formatVersionNumber            = 0x" << HEX(m_FullHeader->generic.formatVersionNumber) << std::endl;
  out << "sourceIdentifier               = 0x" << HEX(m_FullHeader->generic.sourceIdentifier) << std::endl;        
  out << "numberOfStatusElements         = " << m_FullHeader->generic.numberOfStatusElements << std::endl;
  for(u_int loop = 0; loop < m_FullHeader->generic.numberOfStatusElements; loop++)
    out << "statusElement #" << std::setw(3) << loop << "             = 0x" << HEX(m_FullHeader->statusElement[loop]) << std::endl;
  out << "crc_flag                       = " << m_FullHeader->crc_flag << std::endl;
  out << "Specific header:" << std::endl;  
  out << "bcTime1                        = 0x" << HEX(m_FullHeader->bcTime1) << std::endl;
  out << "bcTime2                        = 0x" << HEX(m_FullHeader->bcTime2) << std::endl;
  out << "gid_ls                         = 0x" << HEX(m_FullHeader->gid_ls) << std::endl;
  out << "gid_ms                         = 0x" << HEX(m_FullHeader->gid_ms) << std::endl;
  out << "runType                        = 0x" << HEX(m_FullHeader->runType) << std::endl;
  out << "runNumber                      = 0x" << HEX(m_FullHeader->runNumber) << std::endl;
  out << "luminosityBlockNumber          = 0x" << HEX(m_FullHeader->luminosityBlockNumber) << std::endl;
  out << "l1id                           = 0x" << HEX(m_FullHeader->l1id) << std::endl;
  out << "bcid                           = 0x" << HEX(m_FullHeader->bcid) << std::endl;
  out << "level1tt                       = 0x" << HEX(m_FullHeader->level1tt) << std::endl;
  out << "compressionType                = 0x" << HEX(m_FullHeader->compressionType) << std::endl;
  out << "uncompressedPayloadSize        = 0x" << HEX(m_FullHeader->uncompressedPayloadSize) << std::endl;
  out << "numberOfLevel1TriggerInfoWords = 0x" << HEX(m_FullHeader->numberOfLevel1TriggerInfoWords) << std::endl;
  out << "numberOfLevel2TriggerInfoWords = 0x" << HEX(m_FullHeader->numberOfLevel2TriggerInfoWords) << std::endl;
  out << "numberOfEventFilterInfoWords   = 0x" << HEX(m_FullHeader->numberOfEventFilterInfoWords) << std::endl;    out << "numberOfStreamTagWords         = 0x" << HEX(m_FullHeader->numberOfStreamTagWords) << std::endl;                     
}


/********************************************************************/
bool FullFragment::selectionCriteriaMatch(emon::SelectionCriteria *sc) 
/********************************************************************/
{
  DEBUG_TEXT(DFDB_ROSEF, 15, "FullFragment::selectionCriteriaMatch called");

  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: sc->lvl1_trigger_type.value    = " << HEX(sc->m_lvl1_trigger_type.m_value));
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: sc->status_word.value          = " << HEX(sc->m_status_word.m_value));
  
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: m_FullHeader->level1tt         = " << HEX(m_FullHeader->level1tt));
  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: m_FullHeader->statusElement[0] = " << HEX(m_FullHeader->statusElement[0]));

  if(!sc->m_lvl1_trigger_type.m_ignore && ((u_int)sc->m_lvl1_trigger_type.m_value != m_FullHeader->level1tt))
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: Level 1 Trigger Type does not match. I return false");
    return(false);
  }

  if(sc->m_status_word.m_ignore)
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: Status word ignored. No mismatch on other parameters. I return true");
    return(true);
  }

  if((u_int)sc->m_status_word.m_value != 0xffffffff && (u_int)sc->m_status_word.m_value == m_FullHeader->statusElement[0])
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: Status Word of fragment matches. I return true");
    return(true);
  }

  if((u_int)sc->m_status_word.m_value == 0xffffffff && m_FullHeader->statusElement[0] != 0)
  {
    DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: Status Word of fragment is not zero. I return true");
    return(true);
  }

  DEBUG_TEXT(DFDB_ROSEF, 20, "FullFragment::selectionCriteriaMatch: No match");
  return(false);
}


/********************************/
void callback(ISCallbackInfo *isc) 
/********************************/
{
  // Read info from IS
  LumiBlock lb;
  if (isc->reason() != is::Deleted) 
  {
    isc->value(lb);
    S_LumiBlockNumber = lb.LumiBlockNumber;
    
    //std::cout << "New LumiBlock = " << S_LumiBlockNumber << std::endl;   
  }
  else 
    return;
}

 
/********************************/
void FullFragment::init_lumi(void)
/********************************/
{
  
  try
  {
    int argc = 0;
    char **argv=0;
    IPCCore::init(argc, argv);
  }
  catch(daq::ipc::CannotInitialize& e)
  {
    ers::fatal(e);
    abort();
  }
  catch(daq::ipc::AlreadyInitialized& e)
  {
  }
 
  try
  {
    std::string partition;
    if (std::getenv("ATLAS")) 
    {
      partition = std::getenv("ATLAS");
      std::cout << "TDAQ_PARTITION is " << partition << std::endl;
    }
    else
    {
      std::cout << "ERROR: TDAQ_PARTITION is not defined" << std::endl;
      lumi_init = 2;
      return; 
    }
    IPCPartition part(partition);
    infoRec = new ISInfoReceiver(part);
  }
  catch(daq::ipc::Exception & e)
  {
    std::cout << "ERROR: failed to find partition ATLAS" << std::endl;
    ers::fatal(e);
    lumi_init = 2;
    return;
  }
    
  try 
  {
    infoRec->subscribe("RunParams.LuminosityInfo", callback);
  }
  catch (daq::is::Exception & ex) 
  {
    std::cout << "ERROR: subscribe failed" << std::endl;
    ers::fatal(ex);
    lumi_init = 2;
    return;
  }

  lumi_init = 1;
}


/**************************/
int FullFragment::get_lumi()
/**************************/
{
  if (lumi_init == 1)
  {
    //std::cout << "LumiBlock for this fragment = " << S_LumiBlockNumber << std::endl;   
    return S_LumiBlockNumber;
  }
  else
    return 0;
}


