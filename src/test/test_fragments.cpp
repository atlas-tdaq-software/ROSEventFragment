/****************************************************************/
/*								*/
/*  file test_fragments.cpp					*/
/*								*/
/*  exerciser for the EventFragment class			*/
/*								*/
/*  The program allows to:					*/
/*  . exercise the methods of the EventFragment Class		*/
/*  . measure the performance					*/
/*								*/
/*								*/
/***C 2003 - A nickel program worth a dime***********************/

#include <iostream>
#include <iomanip>
#include <emon/SelectionCriteria.h>
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "DFDebug/DFDebug.h"
#include "ROSMemoryPool/MemoryPool_CMEM.h"
#include "ROSEventFragment/FullFragment.h"
#include "ROSEventFragment/ROBFragment.h"

using namespace ROS;

enum 
{ 
  CREATEROBEMUL=1,
  CREATEROBMISSING,
  CREATEFULL,
  APPENDROBFULL,
  APPENDFULLFULL,
  FILLROB,
  SLIDASROB,
  DUMPROB,
  DUMPFULL,
  DUMPFULLROB,
  DUMPFULLFULL,
  RUNNUMBER,
  CHECKROB,
  CHECKFULL,
  CHECKMATCH,
  DELETEROB,
  DELETEFULL,
  TIMING,
  TIMING2,
  HELP,
  SETDEBUG,
  QUIT = 100
};

#define ROB_POOL_NUMB     100      //Max. number of ROB fragments
#define ROB_POOL_SIZE     0x1000   //Max. size of ROB fragments
#define FULL_POOL_NUMB    100      //Max. number of Full fragments
#define FULL_POOL_SIZE    0x100    //Max. size of Full fragments


/************/
int main(void)
/************/ 
{
  int option = 1, num = 0, num2 = 0, quit_flag, sourceid;  
  tstamp ts1, ts2, ts3;
  float delta;
  u_int isOK, sourceId, level1Id, bunchCrossingId, level1TriggerType;
  u_int detectorEventType, numberOfRODStatusElements;
  u_int numberOfDataElements, statusBlockPosition, runNumber;
  u_int dblevel = 0, dbpackage = DFDB_ROSEF;
  ROBFragment* rob_fragment[ROB_POOL_NUMB] = {0};
  ROBFragment *robfragment;
  FullFragment* full_fragment[FULL_POOL_NUMB] = {0};
  FullFragment *fullfragment;
  quit_flag = 0;

  //Create a memory Pool for ROB fragments
  MemoryPool* rob_pool = new MemoryPool_CMEM(ROB_POOL_NUMB, ROB_POOL_SIZE);
  
  //Create a memory Pool for Full headers
  MemoryPool* full_pool = new MemoryPool_CMEM(FULL_POOL_NUMB, FULL_POOL_SIZE);
  
  do 
  {
  std::cout << std::endl << std::endl;
  std::cout << " Create ROB fragment (emulation mode)            : " << std::dec << CREATEROBEMUL << std::endl; 
  std::cout << " Create ROB fragment (missing event)             : " << std::dec << CREATEROBMISSING << std::endl;
  std::cout << " Create Full fragment                            : " << std::dec << CREATEFULL << std::endl;
  std::cout << " Append ROB fragment to Full fragment            : " << std::dec << APPENDROBFULL << std::endl;
  std::cout << " Append Full fragment to Full fragment           : " << std::dec << APPENDFULLFULL << std::endl;
  std::cout << " Fill data area of ROD fragment in ROB fragment  : " << std::dec << FILLROB  << std::endl;
  std::cout << " Emulate SLIDAS format in ROB fragment           : " << std::dec << SLIDASROB  << std::endl;
  std::cout << " Dump parameters of ROB fragment                 : " << std::dec << DUMPROB << std::endl;
  std::cout << " Dump parameters of Full fragment                : " << std::dec << DUMPFULL << std::endl;
  std::cout << " Dump a full ROB fragment                        : " << std::dec << DUMPFULLROB << std::endl;
  std::cout << " Dump a full Full fragment                       : " << std::dec << DUMPFULLFULL << std::endl;
  std::cout << " Check the runNumber methods                     : " << std::dec << RUNNUMBER << std::endl;
  std::cout << " Check the format of a ROB fragment              : " << std::dec << CHECKROB << std::endl;
  std::cout << " Check the format of a Full fragment             : " << std::dec << CHECKFULL << std::endl;
  std::cout << " Test selectionCriteriaMatch() for Full fragment : " << std::dec << CHECKMATCH << std::endl;
  std::cout << " Delete a ROB fragment                           : " << std::dec << DELETEROB << std::endl;
  std::cout << " Delete a FULL fragment                          : " << std::dec << DELETEFULL << std::endl;
  std::cout << " TIMING new/delete                               : " << std::dec << TIMING << std::endl;
  std::cout << " TIMING append                                   : " << std::dec << TIMING2 << std::endl;
  std::cout << " HELP                                            : " << std::dec << HELP << std::endl;
  std::cout << " SETDEBUG                                        : " << std::dec << SETDEBUG << std::endl;
  std::cout << " QUIT                                            : " << std::dec << QUIT << std::endl;
  std::cout << " ? : ";
  option = getdecd(option);    //get an integer
  switch(option)   
  {
    case CREATEROBEMUL:
      std::cout << "Enter the array index (0.." << ROB_POOL_NUMB - 1 << ")" << std::endl;
      num = getdecd(num);
      if (rob_fragment[num] != 0)
      {
        std::cout << "This array element has already been filled"  << std::endl;
        break;
      }
      
      std::cout << "Enter the Source ID"  << std::endl;
      sourceId = gethexd(0x112233);
      std::cout << "Enter the Level 1 ID"  << std::endl;
      level1Id = gethexd(0x1);
      std::cout << "Enter the Bunch crossing ID"  << std::endl;
      bunchCrossingId = gethexd(0x33);
      std::cout << "Enter the Level 1 trigger type"  << std::endl;
      level1TriggerType = gethexd(0x44);
      std::cout << "Enter the Detector event type"  << std::endl;
      detectorEventType = gethexd(0x5566);
      std::cout << "Enter the Number of ROD status elements"  << std::endl;
      numberOfRODStatusElements = gethexd(1);
      std::cout << "Enter the Number of ROD data elements"  << std::endl;
      numberOfDataElements = gethexd(100);
      std::cout << "Enter the Status block position"  << std::endl;
      statusBlockPosition = gethexd(0);
      std::cout << "Enter the run Number"  << std::endl;
      runNumber = getdecd(0);      

      // The next two calls should be protected by a mutex. 
      // As this test programs can not have race conditions I leave it out.
      robfragment = new ROBFragment(rob_pool, sourceId, level1Id,  bunchCrossingId, level1TriggerType, 
                                    detectorEventType,  numberOfRODStatusElements, numberOfDataElements,
                                    statusBlockPosition, runNumber);
                  
      rob_fragment[num] = robfragment;
      std::cout << "ROB fragment created" << std::endl;   
      break;

    case CREATEROBMISSING:
      std::cout << "Enter the array index (0.." << ROB_POOL_NUMB-1 << ")" << std::endl;
      num = getdecd(num);
      if (rob_fragment[num] != 0)
      {
        std::cout << "This array element has already been filled"  << std::endl;
        break;
      }

      std::cout << "Enter the Source ID"  << std::endl;
      sourceId = gethexd(0x112233);
      std::cout << "Enter the Level 1 ID"  << std::endl;
      level1Id = gethexd(0x1);
      std::cout << "Enter the run Number"  << std::endl;
      runNumber = getdecd(0);  
      
      // The next two calls should be protected by a mutex.
      // As this test programs can not have race conditions I leave it out.
      robfragment = new ROBFragment(rob_pool, level1Id, sourceId, runNumber);

      rob_fragment[num] = robfragment;
      std::cout << "ROB fragment created" << std::endl;
      break;
      
    case CREATEFULL:      
      std::cout << "Enter the array index (0.." << FULL_POOL_NUMB - 1 << ")" << std::endl;
      num = getdecd(num);
      
      std::cout << "Enter the Run number" << std::endl;
      runNumber = getdecd(0);
      
      std::cout << "Enter the Source Identifier" << std::endl;
      sourceid = getdecd(0);
            
      if (full_fragment[num] != 0)
      {
        std::cout << "This array element has already been filled"  << std::endl;
        break;
      }
      
      // The next two calls should be protected by a mutex. 
      // As this test programs can not have race conditions I leave it out.
      fullfragment = new FullFragment(full_pool, runNumber, sourceid);

      full_fragment[num] = fullfragment;
      std::cout << "Full fragment created" << std::endl;      
      break;
      
    case APPENDROBFULL:
      std::cout << "Enter the handle of the Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      std::cout << "Enter the handle of the ROB fragment to append" << std::endl;
      num2 = getdecd(num2);
      if (rob_fragment[num2] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
            
      try 
      {
        full_fragment[num]->append(rob_fragment[num2]);
      }
      catch (EventFragmentException& e) 
      {
         std::cout << e << std::endl;
         std::cout << "Package:     " << e.getPackage() << std::endl 
	      << "Error Id:    " << e.getErrorId() << std::endl 
	      << "Description: " << e.getDescription() << std::endl;
      }
    
      std::cout << "ROB fragment has been appended" << std::endl;
      break;

           
    case APPENDFULLFULL:
      std::cout << "Enter the handle of the master Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      std::cout << "Enter the handle of the Full fragment to append" << std::endl;
      num2 = getdecd(num2);
      if (full_fragment[num2] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
            
      try 
      {
        full_fragment[num]->append(full_fragment[num2]);
      }
      catch (EventFragmentException& e) 
      {
         std::cout << e << std::endl;
         std::cout << "Package:     " << e.getPackage() << std::endl 
	      << "Error Id:    " << e.getErrorId() << std::endl 
	      << "Description: " << e.getDescription() << std::endl;
      }
    
      std::cout << "Full fragment has been appended" << std::endl;
      break;

    case FILLROB:
      std::cout << "Enter the handle of the ROB fragment to process" << std::endl;
      num2 = getdecd(num2);
      if (rob_fragment[num2] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      rob_fragment[num]->fill_data();
      break;

    case SLIDASROB:
      std::cout << "Enter the handle of the ROB fragment to process" << std::endl;
      num2 = getdecd(num2);
      if (rob_fragment[num2] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      rob_fragment[num]->emulate_SLIDAS();
      std::cout << "SLIDAS format generated" << std::endl;
      break;

    case DUMPROB:
      std::cout << "Enter the handle of the ROB fragment" << std::endl;
      num = getdecd(num);
      if (rob_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      
      std::cout << "The 'size' method returns " << rob_fragment[num]->size() << std::endl;
      std::cout << "The 'l1Id' method returns " << rob_fragment[num]->level1Id() << std::endl;
      std::cout << "The 'status' method returns " << rob_fragment[num]->status() << std::endl;
      break;
            
    case DUMPFULL:
      std::cout << "Enter the handle of the Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      
      std::cout << "The 'size' method returns " << full_fragment[num]->size() << std::endl;
      std::cout << "The 'level1Id' method returns " << full_fragment[num]->level1Id() << std::endl;
      std::cout << "The 'status' method returns " << full_fragment[num]->status() << std::endl;
      break;   
      
    case DUMPFULLROB:
      std::cout << "Enter the handle of the ROB fragment" << std::endl;
      num = getdecd(num);
      if (rob_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      rob_fragment[num]->print();
      break;
      
    case DUMPFULLFULL:
      std::cout << "Enter the handle of the Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      full_fragment[num]->print();
      break;

    case RUNNUMBER:
      std::cout << "Enter the handle of the ROB fragment (-1 to skip)" << std::endl;
      num = getdecd(num);
      if (num != -1)
      {
        runNumber = rob_fragment[num]->runNumber();
        std::cout << "The run number of the ROB fragment is " << runNumber << std::endl;
      }
      
      std::cout << "Enter the handle of the Full fragment (-1 to skip)" << std::endl;
      num = getdecd(num);
      if (num != -1)
      {
        runNumber = full_fragment[num]->runNumber();
        std::cout << "The run number of the Full fragment is " << runNumber << std::endl;
      }      
      break;
            
    case CHECKROB:
      std::cout << "Enter the handle of the ROB fragment" << std::endl;
      num = getdecd(num);
      if (rob_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      isOK = rob_fragment[num]->check(0, 0);
      if (isOK)
        std::cout << "The format check returned error " << isOK;
      break;
      
    case CHECKFULL:
      std::cout << "Enter the handle of the Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      isOK = full_fragment[num]->check();
      if (isOK)
        std::cout << "The format check returned error " << isOK;
      break;      

    case CHECKMATCH:
    {
      std::cout << "Enter the handle of the Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      
      emon::L1TriggerType    *l1tt = new emon::L1TriggerType();
      emon::SmartBitValue    *sbit = new emon::SmartBitValue();
      emon::SmartStreamValue *sval = new emon::SmartStreamValue();
      emon::StatusWord       *swrd = new emon::StatusWord();

      int ignore;
      std::cout << "Ignore trigger type (1=yes, 0=no)" << std::endl;
      ignore = getdecd(0);
      
      if (ignore)
        l1tt->m_ignore = true;
      else
      {
        std::cout << "Enter the match pattern for <lvl1_trigger_type> " << std::endl;
        char sc_lvl1_trigger_type = gethexd(0);
        l1tt->m_ignore = false;
        l1tt->m_value = sc_lvl1_trigger_type;
      }
      
      std::cout << "Ignore status word (1=yes, 0=no)" << std::endl;
      ignore = getdecd(0);
      
      if (ignore)
        swrd->m_ignore = true;
      else
      {
	std::cout << "Enter the match pattern for <status_word> " << std::endl;
	int sc_status_word = gethexd(0);
	swrd->m_ignore = false;
	swrd->m_value = sc_status_word;
      }
      	
      emon::SelectionCriteria *sc = new emon::SelectionCriteria(*l1tt, *sbit, *sval, *swrd);	    
	    
      bool is_match = full_fragment[num]->selectionCriteriaMatch(sc);
      if(is_match)
        std::cout << "The fragment matches the criteria" << std::endl;
      else
        std::cout << "The fragment does not match the criteria" << std::endl;

      delete l1tt;
      delete sbit;
      delete sval;
      delete swrd;
      delete sc;
    }  
    break;      

    case DELETEROB:
      std::cout << "Enter the handle of the ROB fragment" << std::endl;
      num = getdecd(num);
      if (rob_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      delete rob_fragment[num];
      rob_fragment[num] = 0;
      break;
            
    case DELETEFULL:
      std::cout << "Enter the handle of the Full fragment" << std::endl;
      num = getdecd(num);
      if (full_fragment[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      delete full_fragment[num];
      full_fragment[num] = 0;
      break;

    case TIMING:
      ts_open(1, TS_DUMMY);
      ts_clock(&ts1);

      for (int loop = 0; loop < 100; loop++) 
      {   // just to make time longer ..
        robfragment = new ROBFragment(rob_pool,0,0,0);
        delete robfragment;
      }

      ts_clock(&ts2);
      delta = ts_duration(ts1, ts2);
      std::cout << "Result for ROB buffer:" << std::endl;
      std::cout << " # seconds per create/destroy pair= " << delta/100.0 << std::endl;
      
      ts_close(TS_DUMMY);
      break;

    case TIMING2:
      int loop2;
      
      ts_open(1, TS_DUMMY);
 
      for (loop2 = 1; loop2 < 20; loop2++)
      {
        for (int loop = 0; loop < loop2; loop++)
        {
          robfragment = new ROBFragment(rob_pool ,0 ,0 ,0);
          rob_fragment[loop] = robfragment;
        }

        ts_clock(&ts1);
        fullfragment = new FullFragment(full_pool ,0 ,0);
        for (int loop = 0; loop < loop2; loop++)    
        {
          try 
          {
            fullfragment->append(rob_fragment[loop]);
          }
          catch (EventFragmentException& e) 
          {
            std::cout << e << std::endl;
            std::cout << "Package:     " << e.getPackage() << std::endl << "Error Id:    " << e.getErrorId() << std::endl << "Description: " << e.getDescription() << std::endl;        
	  }
        }        

        ts_clock(&ts2);
        
        for (int loop = 0; loop < loop2; loop++)
          delete rob_fragment[loop];      
        delete fullfragment;          
         
        ts_clock(&ts3);

        std::cout << " Number of ROB fragments               = " << loop2 << std::endl;
        delta = ts_duration(ts1, ts2);
        std::cout << " Time (us) for new Full & append N*ROB = " << delta * 1000000 << std::endl;
        delta = ts_duration(ts2, ts3);
        std::cout << " Time (is) for delete N*ROB & Full     = " << delta * 1000000 << std::endl;
      }
      
      ts_close(TS_DUMMY);
      break;

    case HELP:
      std::cout <<  " Exerciser program for the EventFragment Class." << std::endl;
      std::cout <<  " The program allows to manipulate ROD, ROB and Full fragments." << std::endl;
      std::cout <<  " Fragments can be created and deleted" << std::endl;
      std::cout <<  " and ROB fragments can be appended to Full fragments." << std::endl;
      std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
      std::cout <<  " the basic EventFragment methods: this is a bit for specialists ..." << std::endl;
      break;

    case SETDEBUG:
      std::cout << "Enter the new debug level " << std::endl;
      dblevel = getdecd(dblevel);
      std::cout << "Enter the new debug package " << std::endl;
      dbpackage = getdecd(dbpackage);
      DF::GlobalDebugSettings::setup(dblevel, dbpackage);
      break;

    case QUIT:
      quit_flag = 1;
      break;

    default:
      std::cout <<  "not implemented yet" << std::endl;
 
    } //main switch
  } while (quit_flag == 0);
  return 0;
}
